-- Step 1: Create Physical Database and Schema
CREATE DATABASE mountaineering club;
\c mountaineering club;
CREATE SCHEMA climbing_schema;

-- Step 2: Create Tables
CREATE TABLE climbing_schema.Mountains (
    MountainID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Height INT,
    Country VARCHAR(255),
    Area VARCHAR(255)
);

CREATE TABLE climbing_schema.Guides (
    GuideID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Address VARCHAR(255),
    Country VARCHAR(255)
);

CREATE TABLE climbing_schema.Climbs (
    ClimbID SERIAL PRIMARY KEY,
    StartDate DATE NOT NULL DEFAULT CURRENT_DATE,
    EndDate DATE,
    MountainID INT NOT NULL,
    GuideID INT NOT NULL,
    CONSTRAINT fk_mountains FOREIGN KEY (MountainID) REFERENCES climbing_schema.Mountains(MountainID),
    CONSTRAINT fk_guides FOREIGN KEY (GuideID) REFERENCES climbing_schema.Guides(GuideID)
);

CREATE TABLE climbing_schema.Climbers (
    ClimberID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Address VARCHAR(255),
    Country VARCHAR(255)
);


CREATE TABLE climbing_schema.Climbers_Climbs (
    ClimberClimbID SERIAL PRIMARY KEY,
    ClimberID INT NOT NULL,
    ClimbID INT NOT NULL,
    CONSTRAINT fk_climbers FOREIGN KEY (ClimberID) REFERENCES climbing_schema.Climbers(ClimberID),
    CONSTRAINT fk_climbs FOREIGN KEY (ClimbID) REFERENCES climbing_schema.Climbs(ClimbID)
);

CREATE TABLE climbing_schema.Gear (
    GearID SERIAL PRIMARY KEY,
    GearName VARCHAR(255) NOT NULL,
    Description TEXT
);

CREATE TABLE climbing_schema.Climb_Gear (
    ClimbGearID SERIAL PRIMARY KEY,
    ClimbID INT NOT NULL,
    GearID INT NOT NULL,
    Quantity INT,
    CONSTRAINT fk_climb_gear_climbs FOREIGN KEY (ClimbID) REFERENCES climbing_schema.Climbs(ClimbID),
    CONSTRAINT fk_climb_gear_gear FOREIGN KEY (GearID) REFERENCES climbing_schema.Gear(GearID)
);

CREATE TABLE climbing_schema.Routes (
    RouteID SERIAL PRIMARY KEY,
    RouteName VARCHAR(255) NOT NULL,
    DifficultyLevel VARCHAR(50)
);

CREATE TABLE climbing_schema.Climb_Routes (
    ClimbRouteID SERIAL PRIMARY KEY,
    ClimbID INT NOT NULL,
    RouteID INT NOT NULL,
    CONSTRAINT fk_climb_routes_climbs FOREIGN KEY (ClimbID) REFERENCES climbing_schema.Climbs(ClimbID),
    CONSTRAINT fk_climb_routes_routes FOREIGN KEY (RouteID) REFERENCES climbing_schema.Routes(RouteID)
);

CREATE TABLE climbing_schema.Events (
    EventID SERIAL PRIMARY KEY,
    EventName VARCHAR(255) NOT NULL,
    EventDate DATE
);

-- Step 3: Apply Check Constraints
ALTER TABLE climbing_schema.Climbs
ADD CONSTRAINT start_date_check CHECK (StartDate > '2000-01-01');

-- Step 4: Populate Tables with Sample Data

-- Climbers Table
INSERT INTO climbing_schema.Climbers (Name, Address, Country) VALUES
('John Doe', '123 Main St', 'USA'),
('Jane Smith', '456 Elm St', 'Canada');

-- Mountains Table
INSERT INTO climbing_schema.Mountains (Name, Height, Country, Area) VALUES
('Mount Everest', 8848, 'Nepal', 'Asia'),
('K2', 8611, 'Pakistan', 'Asia');

-- Guides Table
INSERT INTO climbing_schema.Guides (Name, Address, Country) VALUES
('Mike Johnson', '789 Oak St', 'USA'),
('Sarah Lee', '101 Pine St', 'Canada');

-- Climbs Table
INSERT INTO climbing_schema.Climbs (StartDate, EndDate, MountainID, GuideID) VALUES
('2023-03-15', '2023-03-20', 1, 1),
('2023-04-10', '2023-04-15', 2, 2);

-- Climbers_Climbs Table
INSERT INTO climbing_schema.Climbers_Climbs (ClimberID, ClimbID) VALUES
(1, 1),
(2, 1),
(1, 2);

-- Gear Table
INSERT INTO climbing_schema.Gear (GearName, Description) VALUES
('Climbing Shoes', 'Durable and comfortable shoes for climbing'),
('Harness', 'Safety equipment for climbers');

-- Climb_Gear Table
INSERT INTO climbing_schema.Climb_Gear (ClimbID, GearID, Quantity) VALUES
(1, 1, 2),
(1, 2, 1),
(2, 1, 1);

-- Routes Table
INSERT INTO climbing_schema.Routes (RouteName, DifficultyLevel) VALUES
('North Col Route', 'Advanced'),
('Abruzzi Spur', 'Difficult');

-- Climb_Routes Table
INSERT INTO climbing_schema.Climb_Routes (ClimbID, RouteID) VALUES
(1, 1),
(2, 2);

-- Events Table
INSERT INTO climbing_schema.Events (EventName, EventDate) VALUES
('Climbing Competition', '2023-05-10'),
('Training Camp', '2023-06-15');

-- Step 5: Add 'record_ts' Field

-- Climbs Table
ALTER TABLE climbing_schema.Climbs
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Climbers Table
ALTER TABLE climbing_schema.Climbers
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Mountains Table
ALTER TABLE climbing_schema.Mountains
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Guides Table
ALTER TABLE climbing_schema.Guides
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Climbers_Climbs Table
ALTER TABLE climbing_schema.Climbers_Climbs
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Gear Table
ALTER TABLE climbing_schema.Gear
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Climb_Gear Table
ALTER TABLE climbing_schema.Climb_Gear
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Routes Table
ALTER TABLE climbing_schema.Routes
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Climb_Routes Table
ALTER TABLE climbing_schema.Climb_Routes
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Events Table
ALTER TABLE climbing_schema.Events
ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
