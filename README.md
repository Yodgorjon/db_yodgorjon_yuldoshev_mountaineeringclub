# DB_Yodgorjon_Yuldoshev_MountaineeringClub



BUSINESS DESCRIPTION

1.1 BUSINESS BACKGROUND

Our mountaineering club organizes regular climbs for members to various mountains around the world. These climbs involve multiple climbers, guides, gear, and routes, and managing this information efficiently has become crucial for the club's operations.

1.2 PROBLEMS. CURRENT SITUATION

Currently, the club manages climb information manually, leading to data inconsistencies, difficulty in tracking climbers' participation across climbs, and challenges in inventory management for gear and routes.

1.3 THE BENEFITS OF IMPLEMENTING A DATABASE. PROJECT VISION

Implementing a comprehensive database system will streamline climb management, improve data accuracy, enable better tracking of climbers, gear, and routes, optimize inventory management, and provide valuable insights for planning future climbs and club events.

MODEL DESCRIPTION

2.1 DEFINITIONS & ACRONYMS

PK: Primary Key
FK: Foreign Key
2.2 LOGICAL SCHEME

image of the logical data model

2.3 OBJECTS

Table Description

Climbs Table

Table Name	Field Name	Field Description	    Data Type
Climbs	    ClimbID	    Climb identifier, PK	Int
            StartDate	Start date of the climb	Date
            EndDate	    End date of the climb	Date
            MountainID	Mountain identifier, FK	Int
            GuideID	    Guide identifier, FK	Int

Climbers Table

Table Name	Field Name	Field Description	    Data Type
Climbers	ClimberID	Climber identifier, PK	Int
            Name	    Climber's name	        Varchar
            Address	    Climber's address	    Varchar
            Country	    Climber's country	    Varchar

Comments on table relationships:
The "Climbs" table relates to "Mountains" and "Guides" tables through foreign keys for efficient climb management.
Many-to-many relationships are managed through intermediary tables like "Climbers_Climbs" for climbers and climbs, "Climb_Gear" for climbs and gear, and "Climb_Routes" for climbs and routes.

Example with data:
ClimbID	StartDate	EndDate	    MountainID	GuideID
1	    2024-05-15	2024-05-20	101	        201
2	    2024-06-10	2024-06-15	102	        202
3	    2024-07-05	2024-07-10	103	        203
